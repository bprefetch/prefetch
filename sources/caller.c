/*	Caller
	Make data structures
	Loop until Application terminated
		Case
			IF Meet breakpoint
				Do prefetch

			IF MMAP
				Insert breakpoint

			IF FORK
				Turn tracee to child

			IF exit
				Turn tracee to parents
	Loop end
*/
#include <sys/ptrace.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/user.h>		//user_regs_struct
#include <sys/syscall.h>	//SYS_mmap variable
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <libgen.h>			//for Using basename
#include <signal.h>
#include <string.h>			// memcpy()
#include <fcntl.h>
#include <pthread.h>

#include "../header/hash.h"
#include "../header/sequence.h"
#include "../header/structures.h"

#define TARGET_ADDRESS regs.rax+bp_offset
#define BUFFER_SIZE 255
#define PREFETCH_AHEAD	3

void* do_prefetch(void* prefetch_flist) {
	f_list* flist = (f_list*)prefetch_flist;
	f_node* temp_node = flist->head;
	while (temp_node != NULL) {
		int temp_fd = open(temp_node->filepath, O_RDONLY);
		posix_fadvise(temp_fd, temp_node->offset, temp_node->len, POSIX_FADV_WILLNEED);
		close(temp_fd);
		temp_node = temp_node->next;
	}
	pthread_exit((void*)0);
	return NULL;
}

int main(int argc, char *argv[]) {
	struct user_regs_struct regs;
	unsigned long long backup_data = 0;
	unsigned long long bpcode;
	unsigned long long bp_offset;

	int status;
	int bp_counter = 1;
	int insyscall = 0;

	char* app_name = basename(argv[1]);

	r_list* rlist = newRList();
	offset_list* o_list = newOffsetList();

	if (argc < 2) {
		printf("Usage ./caller <target>\n");
		exit(0);
	}

	int cid = fork();
	// Child process
	if (cid == 0) {
		ptrace(PTRACE_TRACEME, 0, 0, 0);
		raise(SIGSTOP);
		execl(argv[1], argv[1], NULL);
	}
	else {
		wait(&status);

		pid_stack* pstack = newPidStack();
		push(pstack, cid);
		int rootpid = cid;

		ptrace(PTRACE_GETREGS, cid, NULL, &regs);
		unsigned long long starting_address = regs.rip;

		char bp_prefix[BUFFER_SIZE] = "/home/shared/prefetch/logs/bp_";
		char pf_prefix[BUFFER_SIZE] = "/home/shared/prefetch/logs/pf_";

		r_list* rlist = newRList();

		// Load bp_log for Distinguish MMAP
		FILE* bp_file = fopen(strcat(bp_prefix, app_name), "r");
		if (bp_file == NULL ) {
			printf("ERROR, bp_fd\n");
			exit(-1);
		}
		char bp_read_buffer[BUFFER_SIZE];

		while ( fgets(bp_read_buffer, BUFFER_SIZE, bp_file)) {
			int bp_md;
			unsigned long long bp_bp_offset;
			sscanf(bp_read_buffer, "%d,%llx\n", &bp_md, &bp_bp_offset);
			offset_node* o_node = newOffsetNode(bp_md, bp_bp_offset);
			appendONode(o_list, o_node);
		}

		// Load prefetch Sequence for Prefetch
		fl_list* fllist = newFLlist();
		FILE* pf_file = fopen(strcat(pf_prefix, app_name), "r");
		if (pf_file == NULL) {
			printf("ERROR, pf_fd\n");
			exit(-1);
		}
		char pf_read_buffer[BUFFER_SIZE];
		while ( fgets(pf_read_buffer, BUFFER_SIZE, pf_file)) {
			int pf_md;
			unsigned long long pf_bp_offset;
			char pf_filepath[255];
			long pf_offset;
			long pf_len;
			memset(pf_filepath, '\0', 255 * sizeof(char));
			sscanf(pf_read_buffer, "%d,%llx,%[^,],%ld,%ld\n", &pf_md, &pf_bp_offset, pf_filepath, &pf_offset, &pf_len);
			f_node* new_fnode = newFNode(pf_filepath, pf_offset, pf_len);
			f_list* temp_flist = getFList(fllist, pf_md, pf_bp_offset);
			if (temp_flist == NULL) {
				f_list* new_flist = newFList(pf_md, pf_bp_offset);
				appendFNode(new_flist, new_fnode);
				appendFList(fllist, new_flist);
			}
			else {
				appendFNode(temp_flist, new_fnode);
			}
		}

		siginfo_t siginfo;

		ptrace(PTRACE_SETOPTIONS, cid, NULL, PTRACE_O_TRACESYSGOOD
		       | PTRACE_O_TRACEFORK
		       | PTRACE_O_TRACEEXEC
		       | PTRACE_O_TRACEVFORK
		       | PTRACE_O_TRACECLONE
		       | PTRACE_O_EXITKILL | PTRACE_O_MMAPTRACE);
		ptrace(PTRACE_SYSCALL, cid, NULL, NULL);

		while (1) {
			// waitpid(cid, &status, 0);
			cid = waitpid(-1, &status, __WALL);

			ptrace(PTRACE_GETREGS, cid, NULL, &regs);

			/* If tracee is terminated */
			if (WIFEXITED(status)) {
				if (cid == rootpid) {
						printf("Root Process is terminated\n");
						break;
				}
				else if (pstack->top->pid == cid) {
					cid = pop(pstack);
				}

			}
			else if (WIFSTOPPED(status)) {
				ptrace(PTRACE_GETSIGINFO, cid, NULL, &siginfo);
				if (siginfo.si_code == SI_KERNEL) {
					printf("BreakPoint[%d], rip-1 = %llu\n", bp_counter, regs.rip - 1);
					bp_counter++;
					r_node* temp_rnode = getRNode(rlist, regs.rip - 1);
					if (temp_rnode != NULL) {
						f_list* prefetch_list = temp_rnode->flist;

						/* Prefetch */
						for(int i = 0; i < PREFETCH_AHEAD; i++){
							pthread_t mythread;
							// if ( pthread_create(&mythread, NULL, do_posix_fadvise, (void*)temp_fnode) < 0) {
							if ( pthread_create(&mythread, NULL, do_prefetch, (void*)prefetch_list) < 0) {
								perror("Error : pthread_create");
							}
							pthread_detach(mythread);
							prefetch_list = prefetch_list->next;
							if(prefetch_list == NULL)
								break;
						}

						/* Restore */
						if (temp_rnode != NULL) {
							ptrace(PTRACE_POKEDATA, cid, regs.rip - 1, temp_rnode->data);
							regs.rip = regs.rip - 1;
							ptrace(PTRACE_SETREGS, cid, NULL, &regs);
						}
						else {
							printf("Restore data Not found\n");
						}
					}
				}
				/* If tracee is created */
				else if ((status >> 8 == (SIGTRAP | (PTRACE_EVENT_FORK << 8)))
				         || (status >> 8 == (SIGTRAP | (PTRACE_EVENT_VFORK << 8)))
				        ) {
					int fpid;
					ptrace(PTRACE_GETEVENTMSG, cid, NULL, &fpid);
					push(pstack, fpid);
					// cid = fpid;
				}
				else if( siginfo.si_signo == SIGSEGV){
					ptrace(PTRACE_DETACH, cid, 0, 0);
				}
				else if (regs.orig_rax == SYS_mmap) {
					if (insyscall == 0) {
						insyscall = 1;
					}
					else {
						/* Catch MMAP with PROC_EXEC */
						if ((regs.rdx & 0x4) != 0) {
							/* 1. Distingush mmap() */
							char buf[255], fname[255];
							sprintf(buf, "/proc/%d/fd/%d", cid, (int)regs.r8);
							memset(fname, '\0', sizeof(fname));
							readlink(buf, fname, sizeof(fname));
							int md = hash(fname);
							offset_node* temp_onode = getONode(o_list, md);
							if (temp_onode != NULL) {
								bp_offset = temp_onode->bp_offset;

								/* 2. Get original instruction */
								backup_data = ptrace(PTRACE_PEEKTEXT, cid, TARGET_ADDRESS, NULL);
								f_list* suitable_flist = getFList(fllist, md, bp_offset);
								if (suitable_flist != NULL) {
									/* 3.Make restore data structure */
									r_node* rnode = newRNode(TARGET_ADDRESS, backup_data, suitable_flist);
									appendRNode(rlist, rnode);

									/* 4. Insert the breakpoint */
									bpcode = (backup_data & 0xffffffffffffff00) | 0xcc;

									ptrace(PTRACE_POKETEXT, cid, TARGET_ADDRESS, bpcode);

									printf("\nINSERT BP\n");
								}
								else {
									printf("ERROR, getFList returns NULL \n");
								}
							}
						}
						insyscall = 0;
					}
				}
			}
			ptrace(PTRACE_SYSCALL, cid, NULL, NULL);
		}
	}
	// Free all allocated memory.
	return 0;
}

