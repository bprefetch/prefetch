#define _GNU_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/types.h>
#include <dlfcn.h>
#include <string.h>
#include <sched.h>
#include <sys/mman.h>
#include <pthread.h>
#include <fcntl.h>
#include <stdarg.h>
#include <sys/time.h>
#include <math.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <linux/fs.h>
#include <linux/fiemap.h>
#include <libgen.h>

static int fd_log;

int __libc_start_main(int (*main) (int, char **, char **), int argc, char **ubp_av,
                      void (*init) (void), void (*fini)(void), void (*rtld_fini)(void), void (*stack_end));
ssize_t read(int fildes, void *buf, size_t nbyte);
size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream);
long long fiemap(int fd, loff_t pos);

int __libc_start_main(int (*main) (int, char **, char **), int argc, char **ubp_av,
                      void (*init) (void), void (*fini)(void), void (*rtld_fini)(void), void (*stack_end)) {
	int (*original__libc_start_main)(int (*main) (int, char **, char **), int argc, char **ubp_av,
	                                 void (*init) (void), void (*fini)(void), void (*rtld_fini)(void), void (*stack_end));
	mlockall(MCL_CURRENT | MCL_FUTURE);

	fd_log = open("/home/shared/prefetch/logs/log", O_WRONLY | O_APPEND | O_CREAT | O_TRUNC, 00666);

	original__libc_start_main = dlsym(RTLD_NEXT, "__libc_start_main");
	return original__libc_start_main(main, argc, ubp_av, init, fini, rtld_fini, stack_end);
}

ssize_t read(int fildes, void *buf, size_t nbyte) {
	ssize_t (*original_read)(int fildes, void *buf, size_t nbyte);
	loff_t pos;
	long long lba_start;
	ssize_t ret;
	pid_t pid;
	char path[128];
	char fname[128];

	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);

	pid = getpid();
	sprintf(path, "/proc/%d/fd/%d", pid, fildes);
	memset(fname, '\0', sizeof(fname));

	readlink(path, fname, sizeof(fname));

	pos = lseek(fildes, 0, SEEK_CUR);

	original_read = dlsym(RTLD_NEXT, "read");
	ret = (*original_read)(fildes, buf, nbyte);

	if (ret > 0) {
		if (strncmp(fname, "pipe:[", 6) != 0 && strncmp(fname, "anon_inode:", 11) != 0 && strncmp(fname, "socket:", 7)) {
			lba_start = fiemap(fildes, pos);
			if (lba_start >= 0)
				dprintf(fd_log, "wrapper-read called:%p,%s,%ld,%d,%d,%lld\n", __builtin_return_address(0), fname, (long)(ts.tv_sec * pow(10, 9) + ts.tv_nsec), (int)pos, (int)ret, lba_start);
		}
	}

	return ret;
}

size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t (*original_fread)(void *ptr, size_t size, size_t nmemb, FILE * stream);
	size_t ret;
	loff_t pos;
	long long lba_start;
	pid_t pid;
	char path[128];
	char fname[128];
	int fildes;

	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);

	fildes = fileno(stream);

	pid = getpid();
	sprintf(path, "/proc/%d/fd/%d", pid, fildes);
	memset(fname, '\0', sizeof(fname));

	readlink(path, fname, sizeof(fname));

	if (fseek(stream, 0, SEEK_CUR) == 0)
		pos = ftell(stream);

	original_fread = dlsym(RTLD_NEXT, "fread");
	ret = (*original_fread)(ptr, size, nmemb, stream);

	if (!feof(stream)) {
		if (strncmp(fname, "pipe:[", 6) != 0 && strncmp(fname, "anon_inode:", 11) != 0 && strncmp(fname, "socket:", 7)) {
			lba_start = fiemap(fildes, pos);
			if (lba_start >= 0)
				dprintf(fd_log, "wrapper-read called:%p,%s,%ld,%d,%d,%lld\n", __builtin_return_address(0), fname, (long)(ts.tv_sec * pow(10, 9) + ts.tv_nsec), (int)pos, (int)(ret * size), lba_start);
		}
	}

	return ret;
}

long long fiemap(int fd, loff_t pos) {
	int blocksize;
	char *fiebuf;	/* fiemap buffer / ioctl argument */
	struct fiemap *fiemap;
	int i;

	uint count = 32;
	fiebuf = malloc(sizeof (struct fiemap) + (count * sizeof(struct fiemap_extent)));

	if (!fiebuf) {
		perror("Could not allocate fiemap buffers");
		exit(1);
	}

	fiemap = (struct fiemap *)fiebuf;
	fiemap->fm_start = 0;
	fiemap->fm_length = ~0ULL;
	fiemap->fm_flags = 0;
	fiemap->fm_extent_count = count;
	fiemap->fm_mapped_extents = 0;

	if (ioctl(fd, FIGETBSZ, &blocksize) < 0) {
		perror("Can't get block size");
		return -1;
	}

	if (ioctl(fd, FS_IOC_FIEMAP, (unsigned long)fiemap) < 0) {
		perror("FIEMAP ioctl failed");
		printf("flags 0x%x\n", fiemap->fm_flags);
		return -1;
	}
	else {
		for (i = 0; i < fiemap->fm_mapped_extents; i++) {
			if ((fiemap->fm_extents[i].fe_logical <= (int)pos / blocksize) && ((fiemap->fm_extents[i].fe_logical + fiemap->fm_extents[i].fe_length) > (int)pos / blocksize)) {
				//return (fiemap->fm_extents[i].fe_physical / blocksize) + ((int)pos / blocksize);
				return (fiemap->fm_extents[i].fe_physical / blocksize);
			}
		}
	}
	return -1;
}
