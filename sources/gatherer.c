#include "../header/hash.h"
#include "../header/dstructure.h"

void createMmapseq(struct user_regs_struct *regs, char *fname, mmap_list *m_list, mmap_node *m_node, long timestamp);
int createReadseq(mmap_list *m_list, char *filename, struct stat *fileStatus, unsigned long long retRead, long timestamp, unsigned long long lba, loff_t file_offset, ssize_t ret_read);
void getFilepath(pid_t pid, int fd, char *filename);
int getTextsecHdr(Elf64_Ehdr *elfHdr, Elf64_Shdr *sectHdr, char *filename);

#define	PTRACE_EVENT_VFORK		SIGTRAP | PTRACE_EVENT_VFORK << 8
#define	PTRACE_EVENT_FORK		SIGTRAP | PTRACE_EVENT_FORK << 8
#define PTRACE_EVENT_CLONE		SIGTRAP | PTRACE_EVENT_CLONE << 8
#define PTRACE_EVENT_EXEC		SIGTRAP | PTRACE_EVENT_EXEC << 8
#define PTRACE_EVENT_VFORK_DONE	SIGTRAP | PTRACE_EVENT_VFORK_DONE << 8
#define PTRACE_EVENT_EXIT		SIGTRAP | PTRACE_EVENT_EXIT << 8

int main(int argc, char *argv[])
{
	pid_t traced_process, rpid;		// Current tracee, root tracee
	struct user_regs_struct regs;	// General purpose registers of tracees

	ssize_t ret_read;	// Return address of read call
	loff_t file_offset;	// Read file offset
	struct stat fileStatus; // File status holding inode number
	struct timespec ts;	// Structure for getting timestamps
	long timeStamp;		// Timestamps

	unsigned long eventmsg;				// To find pids or thread ids of tracee's children
	unsigned long long retRead, lba;
	siginfo_t siginfo;

	/*
	fp_bpseq : a sequence for injecting breakpoint (md, bp_offset)
	fp_pflist : a sequence for prefetching (md, bp_offset, filepath, file_offset, return value of read)
	fp_log : a log file for using to create breakpoint and prefetching sequences
	*/
	FILE *fp_bpseq, *fp_pflist, *fp_log;
	
	mmap_list *m_list;	// Linked list storing information related to mmap system calls
	mmap_node *m_node;	// Nodes of mmap_list
	
	Elf64_Ehdr *elfHdr;		// a structure for reading ELF binary header
	Elf64_Shdr *sectHdr;	// a structure for reading ELF binary section header

	char buf[256];		// a buffer for reading a log file created by libwrapper
	char fname[256];	// character array to store file paths

	int md;					// Message digest of mmapped files
	int wait_status, sig;	// To hold a wait status of tracee
	int insyscall = 0;		// Flag variable to distinguish system call entry and exit
	
	if (argc != 2) {	// Check weather correct or not of user's command
		printf("Usage: ./gatherer <Target app's path>\n");
		return -1;
	}
		
	traced_process = fork();	// Generate a child process (tracee)

	if (traced_process == 0) {	// Child process
		ptrace(PTRACE_TRACEME, 0, NULL, NULL);
		raise(SIGSTOP);
		execl(argv[1], argv[1], NULL);
	}
	else {	// Parent process (tracer)
		wait(&wait_status);
		
		pid_stack *pstack = newPidStack();	// Create and initiate stack for managing all tracees
		push(pstack, traced_process);
		rpid = traced_process;				// Save the root child process's pid (first child, main tracing target)	

		ptrace(PTRACE_SETOPTIONS, traced_process, NULL, PTRACE_O_TRACEFORK |
								PTRACE_O_EXITKILL |
								PTRACE_O_TRACECLONE |
								PTRACE_O_TRACEEXEC |
								PTRACE_O_TRACEEXIT |
								PTRACE_O_TRACESYSGOOD |
								PTRACE_O_TRACEVFORK |
								PTRACE_O_MMAPTRACE);
		ptrace(PTRACE_GETREGS, traced_process, NULL, &regs);
		
		memset(fname, '\0', 256 * sizeof(char));
		strcpy(fname, "/home/shared/prefetch/logs/bp_");
		fp_bpseq = fopen(strcat(fname, basename(argv[1])), "w+");	// create a file of break point sequences

		memset(fname, '\0', 256 * sizeof(char));
		strcpy(fname, "/home/shared/prefetch/logs/pf_");
		fp_pflist = fopen(strcat(fname, basename(argv[1])), "w+");	// create a file of prefetching sequences

		m_list = newMList();	// make a new linked list related to mmap system calls
		
		elfHdr = malloc(sizeof(Elf64_Ehdr));
		sectHdr = malloc(sizeof(Elf64_Shdr));
		
		memset(fname, '\0', 256 * sizeof(char));
		realpath(argv[1], fname);				// get an absolute path of tracee
		getTextsecHdr(elfHdr, sectHdr, fname);	// get information of tracee's ELF text section header

		regs.rax = 0x400000;											// target process's starting address on memory
		regs.rsi = sectHdr->sh_size + (sectHdr->sh_addr) - regs.rax;	// target process's ending address of text section on memory
		
		createMmapseq(&regs, fname, m_list, m_node, timeStamp);	// insert mmaping information to mmap list

		free(elfHdr);
		free(sectHdr);
		
		ptrace(PTRACE_SYSCALL, traced_process, 0, 0); // resume execution of tracee until next system call
		
		do {	// start main loop for tracing the target application (tracee)
			traced_process = waitpid(-1, &wait_status, __WALL);
			ptrace(PTRACE_GETREGS, traced_process, NULL, &regs);	// get current general purpose registers of tracee

			if (WIFSTOPPED(wait_status)) {
				if (((sig = WSTOPSIG(wait_status)) == (SIGTRAP | 0x80)) || (sig == SIGTRAP)) {	// SIGTRAP | 0x80 indicates the signal number of ptrace syscall-stops
					switch (wait_status >> 8) {	// when the tracee generates new child processes or threads
					case PTRACE_EVENT_FORK:
					case PTRACE_EVENT_VFORK:
						ptrace(PTRACE_GETEVENTMSG, traced_process, NULL, &eventmsg);
						push(pstack, (pid_t)eventmsg);	// push pids of parents of the newly generated processes or threads at stack
						printf("\nevent fork, PID : [%ld]\n", eventmsg);
						break;
					case PTRACE_EVENT_CLONE:
						ptrace(PTRACE_GETEVENTMSG, traced_process, NULL, &eventmsg);
						printf("\nevent clone, PID : [%ld]\n", eventmsg);
						break;
					case PTRACE_EVENT_EXEC:
						ptrace(PTRACE_GETEVENTMSG, traced_process, NULL, &eventmsg);
						printf("\nevent exec, PID : [%ld]\n", eventmsg);
						break;
					case PTRACE_EVENT_EXIT:
						if (traced_process == rpid) {	// when the first tracee will be terminated
							fp_log = fopen("/home/shared/prefetch/logs/log", "r");	// open a log file written by libwrapper.so
							if (fp_log == NULL) {
								perror("fopen");
								return -1;
							}
							while (fgets(buf, sizeof(buf), fp_log)) {	// read 1 line from the log in order to make read call sequences
								memset(fname, '\0', 256 * sizeof(char));
								sscanf(buf, "wrapper-%*s called:%llx,%[^,],%ld,%ld,%zd,%lld", &retRead, fname, &timeStamp, &file_offset, &ret_read, &lba);	// parse the log and assign values to variables
								
								createReadseq(m_list, fname, &fileStatus, retRead, timeStamp, lba, file_offset, ret_read);	// create read sequences
							}
							fclose(fp_log);
						}
						break;
					default:
						if ((regs.orig_rax == __NR_mmap) && (regs.r8 != (unsigned int) - 1) && ((regs.rdx & 0x4) == 0x4)) {	// mmap system call (regs.rdx & 0x4 means setting a PROT_EXEC flag)
							if (insyscall == 0) {	// syscall-enter-stop
								clock_gettime(CLOCK_MONOTONIC, &ts);	// get timestamps
								timeStamp = ts.tv_sec * pow(10, 9) + ts.tv_nsec;	// convert timestamp's unit to nano seconds
								insyscall = 1;
							}
							else {	// syscall-exit-stop
								memset(fname, '\0', 256 * sizeof(char));
								getFilepath(traced_process, (int)regs.r8, fname); 	// get a file path (regs.r8 = File Descriptor)
								createMmapseq(&regs, fname, m_list, m_node, timeStamp);	// Create and add a mmap sequence
								insyscall = 0;
							}
						}
						break;
					}
				}
				else {
					if (sig == SIGSEGV) {
						printf("\n##### SIGSEGV caused #####, current PID : %d\n", traced_process);
						ptrace(PTRACE_DETACH, traced_process, 0, 0);
					}
				}
			}
			else if (WIFEXITED(wait_status)) {	// when each tracees exited except the first tracee
				if (traced_process == rpid) {
					break;
				}
				else if (getStackpid(pstack, traced_process) >= 0) {
					printf("child process %d will be terminated\n", traced_process);
					traced_process = pop(pstack);
				}
			}
			ptrace(PTRACE_SYSCALL, traced_process, NULL, NULL); // resume execution untill next syscall-stop
		} while (1);
		
		if (createSequence(m_list, fp_bpseq, fp_pflist) == 0) {	// create breakpoints and prefetching sequences
			fclose(fp_bpseq);
			fclose(fp_pflist);
		}
		else {
			perror("createSequence");
			return -1;
		}
		
		printf("process will be terminated normally\n");
	}
	return 0;
}

/* create mmap sequences
regs.rax = mmap return value (mmapping starting address), regs.rsi = mmapping length
regs.r8 = file descriptor, regs.r9 = mmapping offset */
void createMmapseq(struct user_regs_struct *regs, char *fname, mmap_list *m_list, mmap_node *m_node, long timestamp) {
	int md = hash(fname);	// get message digest from the file path using hash function
	m_node = newMNode(md, regs->rax, regs->rax + regs->rsi, timestamp);	// create mmap sequence node
	appendMNode(m_list, m_node);	// append mmap node to mmap list
	//printf("\nmmap called at 0x%llx, md = %d, fname = %s, ret_mmap = 0x%llx ~ 0x%llx, timestamp = %ld", regs->rip, md, fname, regs->rax, regs->rax + regs->rsi, timestamp);
}

/* Create read sequences
retAddr = return address of read call, lba = starting LBA of file which should be prefetched
ret_read = return value of read call */
int createReadseq(mmap_list *m_list, char *filename, struct stat *fileStatus, unsigned long long retAddr, long timestamp, unsigned long long lba, loff_t file_offset, ssize_t ret_read) {
	unsigned long long bpOffset;
	
	mmap_node *mNode = NULL;
	pf_node *pNode = NULL;

	if ((mNode = getMNode(m_list, retAddr, timestamp)) == NULL) {	// get mmapped file information which include instructions of this read call
		//perror("getMNode");
		return -1;
	}

	stat(filename, fileStatus);
	
	bpOffset = retAddr - (mNode->mmap_start);	// find an offset of injecting breakpoint target address
	
	if (searchpfNode(lba, file_offset, ret_read, mNode->pf_list) == 1) {
		pNode = newpfNode(fileStatus, bpOffset, filename, lba, file_offset, ret_read); // create read sequence node
		addpfNode(mNode->pf_list, pNode); // add read node to prefetching list
		//printf("\nread called at 0x%llx, md = %d, fname = %s, bp_offset = 0x%llx, timestamp = %ld", (mNode->mmap_start) + bpOffset, mNode->md, filename, bpOffset, timestamp);
		return 0;
	}
	else
		return -1;
}

/* Get an absolute file path from proc file system */
void getFilepath(pid_t pid, int fd, char *filename) {
	char *buf = (char *)malloc(256 * sizeof(char));
	sprintf(buf, "/proc/%d/fd/%d", pid, fd);
	memset(filename, '\0', 256 * sizeof(char));
	readlink(buf, filename, 256 * sizeof(char));
}

/* Get a section header of ELF binary's text section */
int getTextsecHdr(Elf64_Ehdr *elfHdr, Elf64_Shdr *sectHdr, char *filename) {
	int idx;
	char *name = NULL;
	
	FILE *fptr = fopen(filename, "rb");
	fread(elfHdr, 1, sizeof(Elf64_Ehdr), fptr);
	
	fseek(fptr, elfHdr->e_shoff + elfHdr->e_shstrndx * sizeof(Elf64_Shdr), SEEK_SET);
	fread(sectHdr, 1, sizeof(Elf64_Shdr), fptr);
	
	name = (char *)malloc(sectHdr->sh_size * sizeof(char));

	fseek(fptr, sectHdr->sh_offset, SEEK_SET);
	fread(name, 1, sectHdr->sh_size, fptr);
	
	for (idx = 0; idx < elfHdr->e_shnum; idx++) {
		fseek(fptr, elfHdr->e_shoff + idx * sizeof(Elf64_Shdr), SEEK_SET);
		fread(sectHdr, 1, sizeof(Elf64_Shdr), fptr);

		if (strncmp(".text", name + sectHdr->sh_name, 6) == 0) {
			fclose(fptr);
			return 0;
		}
	}
	fclose(fptr);
	return -1;
}
