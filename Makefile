all: wrapper caller gatherer
	
wrapper: sources/libwrapper.c
	gcc -fPIC -shared -o libwrapper.so sources/libwrapper.c -ldl -lm
	echo "LD_PRELOAD=./libwrapper.so [target]"

caller: sources/caller.c header/structures.h
	clang -o caller sources/caller.c -g -pthread

prefetch: sources/prefetch.c header/structures.h
	clang -o prefetch sources/prefetch.c -g

prefetch_thread: sources/prefetch_thread.c header/structures.h
	clang -o prefetch_thread sources/prefetch_thread.c -g -pthread

example: examples/dlopener.c examples/dlreader_1.c examples/dlreader_2.c
	clang -o /home/shared/prefetch/dlopener examples/dlopener.c -ldl -g
	clang -o /home/shared/prefetch/dlreader_1.so examples/dlreader_1.c -fPIC -shared -ldl -g
	clang -o /home/shared/prefetch/dlreader_2.so examples/dlreader_2.c -fPIC -shared -ldl -g

gatherer: sources/gatherer.c
	clang -o gatherer sources/gatherer.c -lm -g

clean: 
	rm *.so
	rm caller
	rm gatherer
